import puppeteer from 'puppeteer';

const FIND_WORD = 'analisis';

describe('Home Page', () => {
  it('Should match the snapshot', async (done) => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://127.0.0.1:8081');
    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot();
    await browser.close();
    done();
  });

  it(`Should have the "${FIND_WORD}" word`, async (done) => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://127.0.0.1:8081');

    const findWord = await page.$('#searchString');
    await page.evaluate(el => el.innerHTML, findWord)
      .then(res => expect(String(res).toLowerCase()).toContain(FIND_WORD.toLowerCase()));


    await browser.close();
    done();
  });
});
