# Integration-Test
This is a repository for running integration test with visual regration.

## Table of Contents
1. [Features](#features)
2. [NPM Scripts](#npm-scripts)
3. [Contributing](CONTRIBUTING.md)
4. [Changelog](CHANGELOG.md)
5. [Getting Started](#getting-started)
    6. [Tests with Jest](#running-tests)
    7. [Lint](#lint-the-app)

## Features
* Node6
* Npm as a task/build runner
* [Airbnb CodeStyle](https://github.com/airbnb/javascript)
* Linting with eslint extended with Airbnb config
* Unit tests with Jest and Enzyme
* Code coverage and reporting with Istanbul


## NPM Scripts
* `lint`: Lint the code according to the rules in `package.json`
* `start`: Start a demo webserver for running the fake site.
* `test`: Run unit tests.
* `test:watch`: Run unit tests in watch mode.
* `test:coverage`: Run the test coverage.
* `clean`: Remove dist/ and coverage/ directory


## Getting started
* Install [Node6 or Node8](https://nodejs.org/en/), preferably using [nvm](https://github.com/creationix/nvm)
* Clone this repository: `git clone git@gitlab.com:k010/integration-test.git` (or download zip)
* CD to project directory: `cd integration-test`
* Install the dependencies: `npm install`


### Running tests
Unit tests, uses Jest as test runner.
* `npm run test` for run the test.
* `npm run test:watch` for run the test in watch mode.
* `npm run test:coverage` for run the test with coverage.


### Lint the APP
* `npm run lint` for run the linter with the AirBnB code style.

